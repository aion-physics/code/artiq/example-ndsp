from pyModbusTCP.client import ModbusClient

class Device:

   def __init__(self, host):
      self.host = host

   def ping(self):
      return True

   def get_temperature(self):
      comm = ModbusClient(host=self.host, auto_close=True)
      comm.open()

      # register 289 corresponds to analog input 1, use iTools to find others.
      temperature = comm.read_input_registers(289) / 10
      return temperature
